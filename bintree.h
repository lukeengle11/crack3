// TODO: modify the struct so it holds both the plaintext
// word and the hash.
typedef struct node {
	char *plaintext;
    char *hash;
	struct node *left;
	struct node *right;
} node;

void insert(char *key, char *ihash, node **leaf);

void print(node *leaf);

node *search(char *key, node *leaf);
