#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"
#include "bintree.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Read in the hash file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **read_hashes(char *filename)
{
    FILE *f = fopen(filename, "r");
  
    int size = 100; // Array size
    char **pwds = (char **)malloc(size * sizeof(char *));
    char str[size];
    int i = 0;
    while (fgets(str, 40, f) != NULL)
    {
        str[strlen(str) - 1] = '\0'; //trim off \n
        if (i == size)  // The array is not big enough, increase the size
        {
            size = size + 100;
            char **newarr = (char **)realloc(pwds, size * sizeof(char *));
            if (newarr != NULL) pwds = newarr;
            else exit(1);
        }
        char *newstr = (char *)malloc((strlen(str) + 1) * sizeof(char));
        strcpy(newstr, str);
        pwds[i] = newstr;
        i++;
    }
    if (i == size) // The array is not big enough for a nulled ending, increase the size
    {
        size = size + 1;
        char **newarr = (char **)realloc(pwds, size * sizeof(char *));
        if (newarr != NULL) pwds = newarr;
        else exit(1);
    }
    pwds[i] = NULL; //set the last element of the array to null so we can look through them properly
 
    fclose(f);
    return pwds;
}


// Read in the dictionary file and return the tree.
// Each node should contain both the hash and the
// plaintext word.
node *read_dict(char *filename)
{
    FILE *f = fopen(filename, "r");
    
    node *tree = NULL;
    char str[50];
    while (fgets(str, 50, f) != NULL)
    {
        str[strlen(str) - 1] = '\0'; //trim off \n
        char *newstr = (char *)malloc((strlen(str) + 1) * sizeof(char));
        strcpy(newstr, str);
        char *hash  = (char *)malloc(33 * sizeof(char)); //allocate memory for a hash
        hash = md5(newstr, strlen(newstr)); //hash the password
        insert(newstr, hash, &tree);
    }
    fclose(f);
    return tree;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);

    // Read the dictionary file into a binary tree
    node *dict = read_dict(argv[2]);

    // For each hash, search for it in the binary tree.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    int i = 0;
    while (hashes[i] != NULL)
    {
        node *pass = search(hashes[i], dict);
        if(pass != NULL)
            printf("%s\t%s\n", hashes[i], pass->plaintext);
        i++;
    }
    
    
}
