#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "md5.h"
#include "bintree.h"

void insert(char *key, char *ihash, node **leaf)
{
    // If there is nothing in this leaf, put something in there and create 2 branches
    if (*leaf == NULL)
    {
        *leaf = (node *) malloc(sizeof(node));
        (*leaf)->plaintext = (char *)malloc((strlen(key) + 1) * sizeof(char)); // Allocate memory for the plaintext password
        (*leaf)->plaintext = key;
        (*leaf)->hash = ihash;
        
        /* initialize the children to null */
        (*leaf)->left = NULL;    
        (*leaf)->right = NULL;  
    }
    else if (strcmp(ihash, (*leaf)->hash) < 0)
        insert (key, ihash, &((*leaf)->left) );
    else if (strcmp(ihash, (*leaf)->hash) > 0)
        insert (key, ihash, &((*leaf)->right) );
}

// Prints the tree in alphabetical order
void print(node *leaf)
{
	if (leaf == NULL) return;
	if (leaf->left != NULL) print(leaf->left);
	printf("%s\n", leaf->hash);
	if (leaf->right != NULL) print(leaf->right);
}

// Modify so the key is the hash to search for
node *search(char *key, node *leaf)
{
  if (leaf != NULL)
  {
      if (strcmp(key, leaf->hash) == 0) // We found it!
      {
         return leaf;
      }
      else if (strcmp(key, leaf->hash) < 0)
         return search(key, leaf->left);
      else
         return search(key, leaf->right);
  }
  else return NULL;
}
